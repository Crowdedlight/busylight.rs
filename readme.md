## Busylight
This is a library to control a connected busylight.

### Install
Add the dependency to the ``cargo.toml`` file. 
```toml
busylight = "0.1.0"
```

### Usage
There is examples in the ``example`` folder. A basic example of creating an object, 
setting it to a colour and ringing with a tone is shown below.
```rust
extern crate busylight;

use busylight::{BusyLight};

fn main() {
    let mut bl = BusyLight::new();
    
    bl.light("red");
    
    bl.ring(Tones::TelephoneNordic, 6);
}
```

#### Keepalive
By default the busylight will turn off when there has been no data sent for 30 seconds. 
To keep the busylight on with the last value set, enable keepalive by:
```rust
bl.keepalive_enable()
```
This automatically sends current settings to the busylight every 20seconds. 
To turn off the keepalive use the function:
```rust
bl.keepalive_disable()
```

#### light(color)
To make the busylight light a specific color just use a valid css color.

```rust
bl.light('orange')
```
To turn it off
```rust
bl.stop_light();
```

#### ring(tone, volume)
Make the busylight play a ringtone. All the available tones are defined in a enum and can be 
access through the ``Tones`` enum.   

Volumesteps:
The busylight accepts volume values of 0-7

```rust
bl.ring(Tones::TelephoneNordic, 4)
```
To turn it off
```rust
bl.stop_ring()
```


Ringtones (All available through the enum)
* OpenOffice
* Quiet
* Funky
* FairyTale
* KuandoTrain
* TelephoneNordic
* TelephoneOriginal
* TelephonePickMeUp
* Buzz    (Basically annoying white noise)

