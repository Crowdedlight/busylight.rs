extern crate busylight;

use busylight::{BusyLight, Tones, Positions};
use std::{thread, time};

fn main() {

    let mut bl = BusyLight::new();
    bl.ring(Tones::TelephoneNordic, 6);

    //sleep 5 sec
    let five_seconds = time::Duration::from_secs(5);
    thread::sleep(five_seconds);

    bl.ring(Tones::Funky, 4);

    thread::sleep(five_seconds);

    bl.stop_ring()
}