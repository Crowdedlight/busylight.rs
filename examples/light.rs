extern crate busylight;

use busylight::{BusyLight};
use std::{thread, time};

fn main() {

    let mut bl = BusyLight::new();
    bl.keepalive_enable();

    bl.light("red");
    println!("just set red");

    let one_seconds = time::Duration::from_secs(1);
    let thirty_seconds = time::Duration::from_secs(35);

    //sleep 1 sec
    thread::sleep(one_seconds);
    bl.light("blue");
    println!("just set blue");

    //sleep 1 sec
    thread::sleep(one_seconds);
    bl.light("rgb(0, 255, 0)");
    println!("just set rgb");

    //sleep 1 sec
    thread::sleep(one_seconds);
    bl.light("#0000ff");
    println!("just set hex");

    //sleep 30sec - Light should not go off before changing again, as keepalive should work
    thread::sleep(thirty_seconds);
    println!("stopped sleeping, setting red");
    bl.light("red");

    thread::sleep(one_seconds);
    bl.stop_light();

    println!("stopped light");
    thread::sleep(one_seconds);
}