extern crate busylight;

use busylight::{BusyLight};
use std::{thread, time};

fn main() {

    let mut bl = BusyLight::new();
    bl.keepalive_enable();

    bl.light("red");
    println!("just set red");

    let forty_seconds = time::Duration::from_secs(40);
    thread::sleep(forty_seconds);

    println!("stopping keepalive");
    bl.keepalive_disable();
    println!("disabled keepalive, sleeping to test");

    thread::sleep(forty_seconds);
    println!("stopped program");
}